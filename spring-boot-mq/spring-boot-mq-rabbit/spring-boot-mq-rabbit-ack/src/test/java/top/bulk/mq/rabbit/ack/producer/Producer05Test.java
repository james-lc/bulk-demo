package top.bulk.mq.rabbit.ack.producer;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.concurrent.ListenableFutureCallback;
import top.bulk.mq.rabbit.ack.SpringBootMqRabbitAckApplication;

import javax.annotation.Resource;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author 散装java
 * @date 2023-02-18
 */
@SpringBootTest(classes = SpringBootMqRabbitAckApplication.class)
@Slf4j
class Producer05Test {
    @Resource
    Producer05 producer05;

    @SneakyThrows
    @Test
    void syncSend() {
        String id = UUID.randomUUID().toString();
        producer05.syncSend(id);
        log.info("[test producer05 syncSend][id:{}] 发送成功", id);

        TimeUnit.SECONDS.sleep(2);
    }

    @Test
    void syncSendDefault() throws InterruptedException {
        String id = UUID.randomUUID().toString();
        producer05.syncSendDefault(id);
        log.info("[test producer05 syncSendDefault][id:{}] 发送成功", id);

        TimeUnit.SECONDS.sleep(2);
    }

    @Test
    void asyncSend() throws InterruptedException {
        String id = UUID.randomUUID().toString();

        producer05.asyncSend(id).addCallback(new ListenableFutureCallback<Void>() {

            @Override
            public void onFailure(Throwable e) {
                log.info("[testASyncSend][发送编号：[{}] 发送异常]]", id, e);
            }

            @Override
            public void onSuccess(Void aVoid) {
                log.info("[testASyncSend][发送编号：[{}] 发送成功，发送成功]", id);
            }

        });

        log.info("[test producer05 asyncSend][id:{}] 发送成功", id);

        TimeUnit.SECONDS.sleep(2);
    }
}